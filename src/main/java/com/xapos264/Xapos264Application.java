package com.xapos264;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos264Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapos264Application.class, args);
	}

}
