package com.xapos264.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos264.models.Category;
import com.xapos264.models.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {
	@Query("FROM Variant WHERE categoryId = ?1")
	List<Variant> findByCategoryId(Long categoryId);

	@Query("FROM Variant WHERE lower(variantName) LIKE lower(concat('%', ?1, '%'))")
	List<Variant> searchVariant(String keyword);

}
