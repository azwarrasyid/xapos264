package com.xapos264.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos264.models.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>{
	@Query("FROM Category WHERE lower(categoryName) LIKE lower(concat('%', ?1, '%'))")
	List<Category> searchCategory(String keyword);
	
}
