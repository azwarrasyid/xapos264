package com.xapos264.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos264.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	@Query("FROM Product WHERE lower(productName) LIKE lower(concat('%', ?1, '%'))")
	List<Product> searchProduct(String keyword);
}
