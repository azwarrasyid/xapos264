package com.xapos264.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos264.models.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>{
	
	@Query("FROM OrderDetail WHERE headerId = ?1")
	List<OrderDetail> findByHeader(Long id);

}
