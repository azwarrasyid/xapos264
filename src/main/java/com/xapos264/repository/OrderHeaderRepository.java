package com.xapos264.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos264.models.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long> {
	@Query("SELECT MAX(id) as maxid FROM OrderHeader")
	Long getMaxOrderHeader();
}
