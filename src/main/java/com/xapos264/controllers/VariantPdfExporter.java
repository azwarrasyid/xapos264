package com.xapos264.controllers;

import java.awt.Color;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.xapos264.models.Category;
import com.xapos264.models.Variant;

public class VariantPdfExporter {
	private List<Variant> listVariant;

	public VariantPdfExporter(List<Variant> listVariant) {
		this.listVariant = listVariant;
	}

	private void writeTableHeader(PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.blue);
		cell.setPadding(5);

		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(Color.WHITE);
		
		cell.setPhrase(new Phrase("Category", font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Variant Code", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Variant Name", font));
		table.addCell(cell);
	}

	private void writeTableData(PdfPTable table) {
		for (Variant variant : listVariant) {
			table.addCell(String.valueOf(variant.category.getCategoryName()));
			table.addCell(String.valueOf(variant.getVariantCode()));
			table.addCell(String.valueOf(variant.getVariantName()));
		}
	}

	public void export(HttpServletResponse response) throws DocumentException, IOException {
		Document document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, response.getOutputStream());

		document.open();
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setSize(18);
		font.setColor(Color.BLUE);

		Paragraph p = new Paragraph("List of Variant", font);
		p.setAlignment(Paragraph.ALIGN_CENTER);

		document.add(p);

		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] { 3.5f, 3.5f, 3.5f });
		table.setSpacingBefore(10);

		writeTableHeader(table);
		writeTableData(table);

		document.add(table);
		document.close();
	}
}
