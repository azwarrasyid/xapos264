package com.xapos264.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos264.models.OrderDetail;
import com.xapos264.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	@GetMapping("orderdetailbyorder/{id}")
	public ResponseEntity<List<OrderDetail>> getAllOrderById(@PathVariable("id") Long id){
		try {
			List<OrderDetail> orderDetail = this.orderDetailRepository.findByHeader(id);
			return new ResponseEntity<>(orderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("orderdetail")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail) {
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		if(orderDetail.equals(orderDetailData)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Success", HttpStatus.NO_CONTENT);
		}
	}
}
