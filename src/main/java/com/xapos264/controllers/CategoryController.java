package com.xapos264.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos264.models.Category;
import com.xapos264.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listcategory", listCategory);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexApi() {
			ModelAndView view = new ModelAndView("category/indexapi");
			return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
		if(!result.hasErrors()) {
			this.categoryRepository.save(category);
			return new ModelAndView("redirect:/category/index");
		} else {
			return new ModelAndView("redirect:/category/index");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = this.categoryRepository.findById(id).orElse(null);
		view.addObject("category", category);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
	
}
