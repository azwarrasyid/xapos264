package com.xapos264.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.swing.tree.ExpandVetoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;
import com.xapos264.models.Category;
import com.xapos264.models.Variant;
import com.xapos264.repository.CategoryRepository;
import com.xapos264.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCategoryController {
	
	@Autowired
	private CategoryRepository categoryRepository;
	

	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> category = this.categoryRepository.findAll();
			return new ResponseEntity<>(category, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("category/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id ){
		try {
			Optional<Category> category = this.categoryRepository.findById(id);
			if(category.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception error) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/category")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category){
		Category categoryData = this.categoryRepository.save(category);
		if(categoryData.equals(category)) {
			return new ResponseEntity<Object>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("update/category")
	public ResponseEntity<Object> updateCategory(@RequestBody Category category){
		Long id = category.getId();
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		if(categoryData.isPresent()) {
			category.setId(id);
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Update Success", HttpStatus.OK);
		}  else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("delete/category/{id}")
	public ResponseEntity<Object>  deleteCategory(@PathVariable("id") Long id){
		this.categoryRepository.deleteById(id);
		return new ResponseEntity<>("Delete Success", HttpStatus.OK); 
	}
	
	
	
	@GetMapping("category/exporttopdf")
	public void exportToPdf(HttpServletResponse response) throws DocumentException, IOException{
		response.setContentType("application/pdf");
		
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename= category_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);
		
		List<Category> listCategory = this.categoryRepository.findAll();
		
		CategoryPdfExporter exporter = new CategoryPdfExporter(listCategory);
		exporter.export(response);
		
	}
	
	@GetMapping("category/search/{keyword}")
	public ResponseEntity<List<Category>> getCategoryByName(@PathVariable("keyword") String keyword){
		if(keyword!=null) {
			List<Category> category = this.categoryRepository.searchCategory(keyword);
			return new ResponseEntity<>(category, HttpStatus.OK);
		} else {
			List<Category> category = this.categoryRepository.findAll();
			return new ResponseEntity<>(category, HttpStatus.OK);
		}
	}
}
