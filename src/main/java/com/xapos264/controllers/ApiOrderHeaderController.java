package com.xapos264.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;
import com.xapos264.models.Category;
import com.xapos264.models.OrderHeader;
import com.xapos264.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderHeaderController {
		
	@Autowired
	private OrderHeaderRepository orderHeaderRepository;
	
	@GetMapping("orderheader")
	public ResponseEntity<List<OrderHeader>> getAllOrderHeader(){
		try {
			List<OrderHeader> orderHeader = this.orderHeaderRepository.findAll();
			return new ResponseEntity<>(orderHeader, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("orderheader")
	public ResponseEntity<Object> saveOrderHeader(@RequestBody OrderHeader orderHeader){
		String timeDec = "" + System.currentTimeMillis();
		orderHeader.setReference(timeDec);
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		
		if(orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("orderheadergetmaxid")
	public ResponseEntity<Long> getMaxOrderHeader() {
		try {
			Long orderHeader = this.orderHeaderRepository.getMaxOrderHeader();
			return new ResponseEntity<>(orderHeader, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("update/orderheader")
	public ResponseEntity<Object> updateOrderHeader (@RequestBody OrderHeader orderHeader){
		Long id = orderHeader.getId();
		Optional<OrderHeader> orderHeaderData = this.orderHeaderRepository.findById(id);
		if(orderHeaderData.isPresent()) {
			orderHeader.setId(id);
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<Object>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
		
		
	}
	
	@GetMapping("orderheader/exporttopdf")
	public void exportToPdf(HttpServletResponse response) throws DocumentException, IOException{
		response.setContentType("application/pdf");
		
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename= orderheader_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);
		
		List<OrderHeader> listOrderHeader = this.orderHeaderRepository.findAll();
		
		OrderHeaderPdfExporter exporter = new OrderHeaderPdfExporter(listOrderHeader);
		exporter.export(response);
		
	}
	
}
