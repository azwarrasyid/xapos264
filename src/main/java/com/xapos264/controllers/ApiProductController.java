package com.xapos264.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;
import com.xapos264.models.Category;
import com.xapos264.models.Product;
import com.xapos264.models.Variant;
import com.xapos264.repository.CategoryRepository;
import com.xapos264.repository.ProductRepository;
import com.xapos264.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")

public class ApiProductController {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantRepository variantRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllCategory() {
		try {
			List<Product> product = this.productRepository.findAll();

			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if (product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/product")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		Product productData = this.productRepository.save(product);
		if(productData.equals(product)) {
			return new ResponseEntity<Object>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("update/product")
	public ResponseEntity<Object> productVariant (@RequestBody Product product) {
		Long id = product.getId();
		Optional<Product> productData = this.productRepository.findById(id);
		if(productData.isPresent()) {
			product.setId(id);
			this.productRepository.save(product);
			return new ResponseEntity<Object>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("delete/product/{id}")
	public ResponseEntity<Object>  deleteProduct(@PathVariable("id") Long id){
		this.productRepository.deleteById(id);
		return new ResponseEntity<>("Delete Success", HttpStatus.OK); 
	}
	
	@GetMapping("product/exporttopdf")
	public void exportToPdf(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");

		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename= product_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);

		List<Product> listProduct = this.productRepository.findAll();

		ProductPdfExporter exporter = new ProductPdfExporter(listProduct);
		exporter.export(response);

	}
	
	@GetMapping("product/search/{keyword}")
	public ResponseEntity<List<Product>> getProductByName(@PathVariable("keyword") String keyword){
		if(keyword!=null) {
			List<Product> product = this.productRepository.searchProduct(keyword);
			return new ResponseEntity<>(product, HttpStatus.OK);
		} else {
			List<Product> product = this.productRepository.findAll();
			return new ResponseEntity<>(product, HttpStatus.OK);
		}
	}
}
