package com.xapos264.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos264.models.Category;
import com.xapos264.models.Product;
import com.xapos264.models.Variant;
import com.xapos264.repository.CategoryRepository;
import com.xapos264.repository.ProductRepository;
import com.xapos264.repository.VariantRepository;

@Controller
@RequestMapping("/product/")
public class ProductController {
	
	@Autowired
	public ProductRepository productRepository;
	
	@Autowired
	public VariantRepository variantRepository;
	
	@Autowired
	public CategoryRepository categoryRepository;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("product/index");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listproduct", listProduct);
		
		//get list category
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("category", listCategory);
		
		//get list variant
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("variant", listVariant);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexApi() {
			ModelAndView view = new ModelAndView("product/indexapi");
			return view;
	}
	
	@GetMapping("addformproduct")
	public ModelAndView addformproduct() {
		ModelAndView view = new ModelAndView("product/addformproduct");
		Product product = new Product();
		view.addObject("product", product);
		
		//get list category
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("category", listCategory);
		
		//get list variant
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("variant", listVariant);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Product product, BindingResult result) {
		if(!result.hasErrors()) {
			this.productRepository.save(product);
			return new ModelAndView("redirect:/product/index");
		} else
			return new ModelAndView("redirect:/product/index");
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("product/addformproduct");
		Product product = this.productRepository.findById(id).orElse(null);
		view.addObject("product", product);
		
		//get list category
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("category",listCategory);
		
		//get list variant
		
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("variant",listVariant);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if(id != null) {
			this.productRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/product/index");
	}
	
}
