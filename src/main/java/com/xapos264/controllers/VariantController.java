package com.xapos264.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos264.models.Category;
import com.xapos264.models.Variant;
import com.xapos264.repository.CategoryRepository;
import com.xapos264.repository.VariantRepository;

@Controller
@RequestMapping("/variant/")
public class VariantController {
	
	@Autowired
	public VariantRepository variantRepository;
	
	@Autowired
	public CategoryRepository categoryRepository;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("variant/index");
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listvariant", listVariant);
		
		//get list category
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("category", listCategory);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexApi() {
			ModelAndView view = new ModelAndView("variant/indexapi");
			return view;
	}
	
	@GetMapping("addformvariant")
	public ModelAndView addformvariant() {
		ModelAndView view = new ModelAndView("variant/addformvariant");
		Variant variant = new Variant();
		view.addObject("variant", variant);
		
		
		//get list category
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("category", listCategory);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variant variant, BindingResult result) {
		if(!result.hasErrors()) {
			this.variantRepository.save(variant);
			return new ModelAndView("redirect:/variant/index");
		} else {
			return new ModelAndView("redirect:/variant/index");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("variant/addformvariant");
		Variant variant = this.variantRepository.findById(id).orElse(null);
		view.addObject("variant", variant);
		
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("category", listCategory);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.variantRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/variant/index");
	}
}
