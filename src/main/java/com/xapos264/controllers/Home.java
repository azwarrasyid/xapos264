package com.xapos264.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping
public class Home {
	@GetMapping("index")
	public String index() {
		return "index";
	}
	
	@GetMapping("category")
	public String category() {
		return "category";
	}
	
	@GetMapping("form")
	public String form() {
		return "form";
	}
	
	@GetMapping("kalkulator")
	public String kalkulator() {
		return "kalkulator";
	}
}
