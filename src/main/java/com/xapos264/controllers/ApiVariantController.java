package com.xapos264.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;
import com.xapos264.models.Category;
import com.xapos264.models.Variant;
import com.xapos264.repository.CategoryRepository;
import com.xapos264.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantController {

	@Autowired
	private VariantRepository variantRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllCategory() {
		try {
			List<Variant> variant = this.variantRepository.findAll();

			List<Category> category = this.categoryRepository.findAll();

			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("variant/{id}")
	public ResponseEntity<List<Variant>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			List<Category> category = this.categoryRepository.findAll();
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("variantbycategory/{id}")
	public ResponseEntity<List<Variant>> getAllVariantByCategory(@PathVariable("id") Long id) {
		try {
			List<Variant> variant = this.variantRepository.findByCategoryId(id);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/variant")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {
		Variant variantData = this.variantRepository.save(variant);
		List<Category> category = this.categoryRepository.findAll();
		if (variantData.equals(variant)) {
			return new ResponseEntity<Object>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("update/variant")
	public ResponseEntity<Object> updateVariant(@RequestBody Variant variant) {
		Long id = variant.getId();
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			variant.setId(id);
			this.variantRepository.save(variant);
			return new ResponseEntity<Object>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("delete/variant/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		this.variantRepository.deleteById(id);
		return new ResponseEntity<>("Delete Success", HttpStatus.OK);
	}

	@GetMapping("variant/exporttopdf")
	public void exportToPdf(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");

		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename= variant_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);

		List<Variant> listVariant = this.variantRepository.findAll();

		VariantPdfExporter exporter = new VariantPdfExporter(listVariant);
		exporter.export(response);

	}

	@GetMapping("variant/search/{keyword}")
	public ResponseEntity<List<Variant>> getVariantByName(@PathVariable("keyword") String keyword) {
		if (keyword != null) {
			List<Variant> variant = this.variantRepository.searchVariant(keyword);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} else {
			List<Variant> variant = this.variantRepository.findAll();
			return new ResponseEntity<>(variant, HttpStatus.OK);
		}
	}
}
