package com.xapos264.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/orderdetail/")
public class OrderDetailController {
	@GetMapping(value="indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("orderdetail/indexapi");
		return view;
	}
}
